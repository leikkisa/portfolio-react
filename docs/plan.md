# Portfolio Plan

## MVP
Phase 1

- Single page of content
  - Link to resume
  - Some way to contact me
    - Prefer a form if I can figure out email server
  - Blurb about me
  - Skills list

- Deployment?
- Domain name?

Phase 2
- Navigation/Routing
- Database
- Posts (can use this for projects if need be or blog)
  - Just title and blurb
  - Build API
  - Build Components

Phase 3
- Project component
- Populate projects

Phase 4
- Other nice to haves
  - eslint
  - testing
  - API, or connect to other APIs
  - Photo of myself
  - Hero image

## Big plan
- React starter
- Sections - Use icons in menu?
  - About
  - Projects
  - Posts
  - Contact
- Navigation component
- Menu component
  - Minimal, name, menu (see richardmattka.com)
- Header Component (Hero image with name? Tagline?)
- Footer Component
- Bio (blurb component?)
- Project Component
  - Image
  - Title
  - Description
  - Skill categories
  - Other categories (fun?)
  - Git link
  - Deployed link (if exists)
- Community affiliations?
  - Participate in or have participated in the past
  - Double Union
- Friend affiliations to promote their stuff?
  - Rebecca, Cianna, Patrick
- Super mini blurb about me
  - e.g. Sally Maki is a web developer and data enthusiast in Oakland.
  - Web Developer and Data Analyst
  - Software Engineer and Data Analyst
- Skills list
  - See resume
- Photo of myself
- Link to resume minus contact info
- Contact information or form
- Use eslint
- Webpack config?
- Blog
  - Post component
    - Photo
    - Title
    - Date
    - Blurb
  - Post ideas
    - Computer set up
    - Meta algorithm
    - Health
    - The Zig Zag learning model
    - Silly stuff
- Quotes and or reading list API
- Styling (bootstrap?)
- Testimonials?
- Connect to API somewhere? GitHub? BitBucket? Twitter? Facebook?
- Deployment
- Connect domain name
- Algorithms section?
- What's the story that I want to tell?
- Carousel with projects?
- Feature project?

- Potential Projects to post (check for the ones with cleaner code)
  - KegScout
  - Resource library
  - Algorithm space/time calculator
  - Roam / Vinyl
  - Web scraper?
  - Pokemon?
  - Step?
  - Projects before the Guild?
  - Budget voting app (need to build)
  - ERG Logo?
  - CLI to do list
  - HackerBees?
  - GIS projects
  - Blurbs about data projects from past jobs?

Favorite examples
- jennwil.de
- http://www.benmvp.com/
- Jlord
- http://writeapp.net/mac/
- CloudCity
- Invision
- https://kanadachi.com/

# DONE
- Social media icon links
  - Put near the top
    - Facebook
    - Twitter
    - LinkedIn
    - Google plus?
    - GitHub
    - BitBucket
    - Instagram
- Favicon
- React initial project set up and dependencies
- Bootstrap or other styling
- Express server
- Footer
