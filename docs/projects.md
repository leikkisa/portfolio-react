Project categories
- Code
  - Solo
  - Contribution
- Fun
- Graphic Design
- GIS
- Analytics

Projects
- Roam
  - Description: An Express / EJS app which allows users to add cities and submit reviews for cities.
- Bookstore
  - A simple bookstore featuring pagination
- Personal Software Development Resource Library
  - Skills:
  - Git: https://bitbucket.org/leikkisa/libraries
  - Description: This is a git repo where I build reference resources for myself (also to share for anyone else who is intersted).  This includes code templates for HTML, SQL, JavaScript, config files, and project starts.  It has examples of algorithms and data structures, links, and notes (such as on computer set up or interviewing prep).
- KegScout
  - Description:
- React Redux Books
- React Redux Blog
- React Video Player
  - Description: A simple React/Redux app that shows a list of YouTube videos based on the search term entered by the user.
  - Skills: React, Redux, API
- Retweet Generator
  - Skills: Express, PostgreSQL, Pug, API (External), Babel
  - Description: The Retweet Generator is a bot which interaacts with the Twitter API.  It re-tweets a saved tweet at a set interval.  It has a homepage which shows the user's last 50 tweets, as well as tweets saved in the database.  When the user posts a new tweet, it gets saved in the database.
- IMDB Movie Search CLI
  - Skills: Command Line
- Algorithm Time/Space complexity calculator
  - Skills:
  - Description: When my group started practicing algorithms at Learners Guild, I was curious to see which solutions performed best in terms of time and space required to run. I built a tool that takes inputs of various sizes, runs each of the solutions, and creates graphs of the time/space used by input size.
- To Do List CLI
  - Skills: Command Line
  - Description: This is a simple command line tool for adding tasks to a to do list, listing the tasks, and marking them as complete.
- Hacker Bees ARG/Puzzle Hunt
  - Category: Fun
  - Skills:
  - Description: Hacker Bees is an alternate reality game / puzzle hunt for Maker Faire that I worked on with a core team of three other people, led by Ellen Juhlin.  We created a storyline, partnered with other booths at Maker Faire, and had the participants explore Maker Faire, solve puzzles, interact with actors, and create a badge with a simple circuit.  I contributed to most tasks and was also responsible for laser cutting the badges.  Our exhibit won an Editor's Choice award.
- ERG Alumni Logo
  - Skills: Graphic Design
  - Description: My graduate department did not have a logo, so one day I drew up a doodle for a design, and eventually implemented it with Photoshop.  The department is the Energy and Resources group which is an interdisciplinary group that focuses on clean energy, water, international development, and social justice.  The logo depicts a leaf, a water droplets, a sun, and a person.  The logo is being used for the department's Alumni Network.
- CSSS Tshirt?
  - Skills: Graphic Design
- Step
  - Category: Code > Contributor
  - Skills: React, CSS
  - Description: A To Do list which zooms in to focus on a single task. I contributed to adding drag-and-drop list sorting, testing, and CSS styling.
- WREZ mapping
- Renewable Energy World publication
- Energy Savings Forecast
- CRUD Mutably
  - Skills: React, External API
  - Description: A front-end app which displays a list of Pokemon retrieved from an API and allows users to add, edit or delete the Pokemon.


JSON Example
{
    "glossary": {
        "title": "example glossary",
		"GlossDiv": {
            "title": "S",
			"GlossList": {
                "GlossEntry": {
                    "ID": "SGML",
					"SortAs": "SGML",
					"GlossTerm": "Standard Generalized Markup Language",
					"Acronym": "SGML",
					"Abbrev": "ISO 8879:1986",
					"GlossDef": {
                        "para": "A meta-markup language, used to create markup languages such as DocBook.",
						"GlossSeeAlso": ["GML", "XML"]
                    },
					"GlossSee": "markup"
                }
            }
        }
    }
}
