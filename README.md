# Sally's Portfolio #

This is my personal homepage, with some projects, an about me section, my resume and how to contact me. It's a work in progress!

## Live site
See the live version at [sally.codes](http://sally.codes)
![](portfolio-screenshot.png)

## Development installation instructions

### Prerequisites
- Node.js
- PostgreSQL

### Installation
- clone the repo
- Make a .env file from the .env.template in the root directory
- $ npm install
- $ npm run db:create
- $ npm run db:migrate
- $ webpack
- $ npm run start:dev
- navigate to localhost:3021 in the browser

## Built With
- Node.js
- Express
- React
- Redux
- Materialize
- Font Awesome
- PostgreSQL
