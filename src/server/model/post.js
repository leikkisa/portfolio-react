const db = require('./db/post')

module.exports = {
  create: db.create,
  destroy: db.destroy,
  edit: db.edit,
  show: db.show,
  showLatest: db.showLatest,
  showByPage: db.showByPage,
  index: db.index,
  pageCount: db.pageCount,
  search: db.search,
}
