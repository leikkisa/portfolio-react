const db = require('./db')
const { printDbError } = require('./helpers')

function create(data) {
  const query = `
    INSERT INTO post (title, author, categories, content, photo_url, thumbnail_url) VALUES
    ($1, $2, $3, $4, $5, $6)
    RETURNING *
    ;`
  return db.query(query, [ data.title, data.author, data.categories, data.content, data.photo_url, data.thumbnail_url ])
    .then(res => res.rows[0])
    .catch(err => {
      printDbError(err, arguments)
    })
}

function destroy(id) {
  const query = `DELETE FROM post WHERE id = $1 RETURNING id;`
  return db.query(query, [ id ])
    .then(res => res.rows[0])
    .catch(err => {
      printDbError(err, arguments)
    })
}

function index() {
  const query = `
    SELECT
      p.*
    FROM post p
    ORDER by p.created_date desc
    ;`
  return db.query(query)
    .then(res => res.rows)
    .catch(err => {
      printDbError(err, arguments)
    })
}

function show(id) {
  const query = `
    SELECT
      *
    FROM post p
    WHERE p.id = $1
    ;`
  return db.query(query, [ id ])
    .then(res => res.rows[0])
    .catch(err => {
      printDbError(err, arguments)
    })
}

function showByPage(page = 1, resultsPerPage = 10) {
  const query = `
    SELECT
      p.*
    FROM post p
    ORDER by p.created_date desc
    LIMIT $2 OFFSET $2 * ($1 - 1)
    ;`
  return db.query(query, [ page, resultsPerPage ])
    .then(res => res.rows)
    .catch(err => {
      printDbError(err, arguments)
    })
}

function showLatest(count = 1) {
  const query = `
    SELECT
      p.*
    FROM post p
    ORDER by p.created_date desc
    LIMIT $1
    ;`
  return db.query(query, [ count ])
    .then(res => res.rows)
    .catch(err => {
      printDbError(err, arguments)
    })
}

function update(id, data) {
  const query = `
    UPDATE post
    SET
      title = $2,
      author = $3,
      categories = $4,
      content = $5,
      photo_url = $6,
      thumbnail_url = $7
    WHERE id = $1
    RETURNING *
    ($1, $2, $3, $4, $5, $6, $7)
    ;`
  return db.query(query, [ id, data.title, data.author, data.categories, data.content, data.photo_url, data.thumbnail_url ])
    .then(res => res.rows[0])
    .catch(err => {
      printDbError(err, arguments)
    })
}

module.exports = {
  create,
  destroy,
  index,
  show,
  showByPage,
  showLatest,
  update,
}
