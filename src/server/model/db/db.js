const { Client } = require('pg')
const config = require('../../config').getConfig()

const db = new Client(config.db.url)

db.connect()

module.exports = db
