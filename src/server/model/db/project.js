const db = require('./db')
const { printDbError } = require('./helpers')

function create(data) {
  const query = `
    INSERT INTO project (title, categories, stack, skills, description, photo_url, thumbnail_url, repo_url, live_url, published, sort_order) VALUES
    ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
    RETURNING *
    ;`
  return db.query(query, [ data.title, data.categories, data.stack, data.skills, data.description, data.photo_url, data.thumbnail_url, data.repo_url, data.live_url, data.published, data.sort_order ])
    .then(res => res.rows[0])
    .catch(err => {
      printDbError(err, arguments)
    })
}

function destroy(id) {
  const query = `DELETE FROM project WHERE id = $1 RETURNING id;`
  return db.query(query, [ id ])
    .then(res => res.rows[0])
    .catch(err => {
      printDbError(err, arguments)
    })
}

function index() {
  const query = `
  SELECT * FROM project
  ORDER BY sort_order, created_date desc
  ;`
  return db.query(query)
    .then(res => res.rows)
    .catch(err => {
      printDbError(err, arguments)
    })
}

function show(id) {
  const query = `
    SELECT
      *
    FROM project p
    WHERE p.id = $1
    ;`
  return db.query(query, [id])
    .then(res => res.rows[0])
    .catch(err => {
      printDbError(err, arguments)
    })
}

function update(id, data) {
  const query = `
    UPDATE projects
    SET
      title = $2,
      categories = $3,
      stack = $4,
      skills = $5,
      description = $6,
      photo_url = $7,
      thumbnail_url = $8,
      repo_url = $9,
      live_url = $10,
      published = $11,
      sort_order = $12
    WHERE id = $1
    RETURNING *
    ;`
  return db.query(query, [ id, data.title, data.categories, data.stack, data.skills, data.description, data.photo_url, data.thumbnail_url, data.repo_url, data.live_url, data.published, data.sort_order ])
    .then(res => res.rows[0])
    .catch(err => {
      printDbError(err, arguments)
    })
}

module.exports = {
  create,
  destroy,
  index,
  show,
  update,
}
