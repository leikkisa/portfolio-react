const db = require('./db/project')

module.exports = {
  create: db.create,
  destroy: db.destroy,
  index: db.index,
  show: db.show,
  update: db.update,
}
