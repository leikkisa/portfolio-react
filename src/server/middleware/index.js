const logErrors = (error, request, response, next) => {
  console.error(error.stack)
  next(error);
}

module.exports = {
  logErrors,
}
