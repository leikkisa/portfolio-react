module.exports = (function() {

  let config = {}

  const getEnv = () => {
    return process.env.NODE_ENV
  }

  const makeConfig = () => {
    if (getEnv() === 'development' ) {
      require('dotenv').config({path: __dirname + '/../../../.env'})
    }

    config = {
      db: {
        url: process.env.DATABASE_URL
      },
      env: process.env.NODE_ENV
    }

    return config
  }

  makeConfig()

  const getConfig = () => {
    return config
  }

  return {
    getConfig,
    getEnv
  }
})()
