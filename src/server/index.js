import express from 'express'
import path from 'path'
import middleware from './middleware'
import router from './router'

const app = express()

if (app.get('env') === 'development') {
  const morgan = require('morgan')
  app.use(morgan('dev'))
}

const assets = express.static(path.join(__dirname, '../../public'))
app.use(assets)
app.use(middleware.logErrors)

app.use('/', router)

const port = process.env.PORT
app.listen(port, () => {
  console.log(`Sally's Portfolio serving on http://localhost:${port}`)
})

export default app
