import React from 'react'
import { renderToNodeStream } from 'react-dom/server'
import express from 'express'
import { StaticRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import promise from 'redux-promise'

import reducers from '../../client/reducers'
import App from '../../client/containers/app'

const router = express.Router()

router.use('*', (request, response) => {

  const createStoreWithMiddleware = applyMiddleware(promise)(createStore)

  const context = {}

  response.write(`<!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="Personal homepage and portfolio for Oakland-based full-stack software engineering student, data analyst, and philosopher.">

      <link rel="manifest" href="manifest.json">
      <link rel="shortcut icon" href="/favicon.ico">
    	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Bellefair|Spectral" rel="stylesheet">
    	<link href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css" rel="stylesheet">
      <link rel="stylesheet" href="/style/style.css">
      <script src="https://use.fontawesome.com/f3570b98e6.js"></script>

      <style>
      @font-face {
        font-family: OptimusPrinceps;
        src: url('/font/OptimusPrinceps.ttf');
      }
      </style>

      <title>Sally Maki</title>
    </head>
    <body>
      <div id="root">`)

  const stream = renderToNodeStream(
    <Provider store={createStore(reducers)}>
    <StaticRouter context={context} location={request.originalUrl} >
    <App />
    </StaticRouter>
    </Provider>
  )

  stream.pipe(response, { end: false, })
  stream.on('end', () => {
    response.write(`</div>
      <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
      <script type="text/javascript" src="/js/bundle.js"></script>
      <script type="text/javascript" src="/js/init.js"></script>
      </body></html>`)
    response.end()
  })

})

export default router
