import express from 'express'

import apiRoutes from './api'
import ssr from './render'

const router = express.Router()

router.use('/api', apiRoutes)
router.use('*', ssr)

module.exports = router
