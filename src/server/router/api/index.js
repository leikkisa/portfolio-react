const router = require('express').Router()
import projectsRouter from './projects'
import postsRouter from './posts'

router.use('/projects', projectsRouter)
router.use('/posts', postsRouter)

router.get('/', (request, response) => {
  response.send('API documentation to go here!')
})

module.exports = router
