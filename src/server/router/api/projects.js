const router = require('express').Router()
const Project = require('../../model/project')

router.get('/', (request, response, next) => {
  Project.index()
  .then((data) => {
    response.json({ data })
  })
  .catch(next)
})

router.get('/:id', (request, response, next) => {
  const id = request.params.id
  Project.show(id)
  .then((data) => {
    response.json({ data })
  })
  .catch(next)
})

module.exports = router
