const router = require('express').Router()
const Post = require('../../model/post')

router.get('/', (request, response, next) => {
  Post.index()
  .then((data) => {
    response.json(data)
  })
  .catch(next)
})

router.get('/latest', (request, response, next) => {
  const count = request.query.count || 3
  Post.showLatest(count)
  .then((data) => {
    response.json(data)
  })
  .catch(next)
})

router.get('/:id', (request, response, next) => {
  const id = request.params.id
  Post.show(id)
  .then((data) => {
    response.json(data)
  })
  .catch(next)
})

module.exports = router
