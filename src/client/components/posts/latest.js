import React, { Component } from 'react'
import moment from 'moment'
import _ from 'lodash'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { fetchLatestPosts } from '../../actions'

class PostsLatest extends Component {
  componentDidMount() {
    this.props.fetchLatestPosts(2)
  }

  renderPosts() {
    return _.map(this.props.posts, post => {
      return (
        <li className="list-group-item" key={post.id}>
          <Link to={`/posts/${post.id}`}>
            {moment(post.created_date).format('MMM Do, YYYY')} - {post.title}
          </Link>
        </li>
      )
    })
  }

  render() {
    return (
      <div>
        <h1>Latest Posts</h1>
        <ul className="list-group">
          {this.renderPosts()}
        </ul>
        <Link to={`/posts`}>
          See all posts &rarr;
        </Link>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {posts: state.posts }
}

export default connect(mapStateToProps, { fetchLatestPosts })(PostsLatest)
