import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import moment from 'moment'
import sanitizeHtml from 'sanitize-html'
import { fetchPost, deletePost } from '../../actions'

class PostsShow extends Component {
  componentDidMount() {
    if (!this.props.post) {
      const { id } = this.props.match.params
      this.props.fetchPost(id)
    }
  }

  onDeleteClick() {
    const { id } = this.props.match.params
    this.props.deletePost(id, () => {
      this.props.history.push('/')
    })
  }

  render() {
    const { post } = this.props

    if (!post) {
      return <div>Loading...</div>
    }

    function createMarkup() {
      return {__html: sanitizeHtml(post.content)};
    }

    return (
      <div className="container">
        <h1>{post.title}</h1>
        <h3>{moment(post.created_date).format('MMM Do, YYYY')}</h3>
        <h6>{post.categories}</h6>
        <div dangerouslySetInnerHTML={createMarkup()} />
        <p><Link to="/posts">See all Posts</Link></p>
      </div>
    )
  }
}

function mapStateToProps({ posts }, ownProps) {
  return { post: posts[ownProps.match.params.id] }
}

export default connect(mapStateToProps, { fetchPost, deletePost })(PostsShow)
