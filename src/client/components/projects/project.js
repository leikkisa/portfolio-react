import React, { Component } from 'react'

export default class Project extends Component {
  render() {
    let demoLink = null
    if(this.props.demo) {
      demoLink = <a href={this.props.demo} target='_blank'><i className='fa fa-globe fa-lg'></i></a>
    }
    return (
      <div className="col s12 m6 l4 xl3">
        <div className="card sticky-action">
          <div className="card-image">
            <img className="activator small" src={this.props.image} />
          </div>
          <div className="card-content small">
            <span className="card-title activator">{this.props.title}<i className="material-icons right">more_vert</i></span>
            <p>{this.props.description}</p>
          </div>
          <div className="card-action">
            <a href={this.props.code} target='_blank'><i className='fa fa-github fa-lg'></i></a>
          {demoLink}
          </div>
          <div className="card-reveal">
            <span className="card-title">{this.props.title}<i className="material-icons right">close</i></span>
            {this.props.reveal}
          </div>
        </div>
      </div>
    )
  }
}
