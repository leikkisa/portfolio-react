import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Navigation from './navigation'
import RainbowHorizontal from './rainbow-horizontal'

export default class Header extends Component {
  render() {
    return (
      <div className='header'>
      <div className='row blue-grey-text'>
        <div className='col s3 header-left'>
          <Link to='/'>
            <h1 className='brand-logo center'>Sally Maki</h1>
          </Link>
        </div>
        <div className='col s6'>
          <h2 className='job-title blue-grey-text'>Full-stack Software Engineer</h2>
        </div>
        <div className='col s3'>
          <Navigation />
        </div>
      </div>
      <RainbowHorizontal />
    </div>
    )
  }
}
