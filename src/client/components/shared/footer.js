import React, { Component } from 'react'
import RainbowHorizontal from './rainbow-horizontal'
import SocialMedia from './social-media'

export default class Footer extends Component {
  render () {
    return (
      <div id="footer">
      <RainbowHorizontal />
      <footer className="page-footer white">
        <div id="page-footer" className="container">
          <SocialMedia />
          <p className="copyright">Do the hard thing.  © 2017</p>
        </div>
      </footer>
      </div>
    )
  }
}
