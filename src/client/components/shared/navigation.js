import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import RainbowHorizontal from './rainbow-horizontal'

export default class Navigation extends Component {
  render() {
    return (
      <div>
        <nav className='z-depth-0'>
          <div className='nav-wrapper white'>
            <Link to='/' className='blue-grey-text brand-logo'>Sally Maki</Link>
            <ul id="nav-desktop" className="right hide-on-med-and-down">
              <li><Link to='/' className='blue-grey-text'>Home</Link></li>
              <li><Link to='/posts' className='blue-grey-text'>Posts</Link></li>
              <li><Link to='/about' className='blue-grey-text'>About</Link></li>
              <li><a href='/file/Sally Maki Resume - Junior Software Developer 2017.pdf' className='blue-grey-text' target='_blank'>Resume</a></li>
              <li><Link to='/contact' className='blue-grey-text'>Contact</Link></li>
            </ul>
            <ul id="nav-mobile" className="side-nav">
              <li><h4>Menu</h4></li>
              <li><Link to='/' className='blue-grey-text'>Home</Link></li>
              <li><Link to='/posts' className='blue-grey-text'>Posts</Link></li>
              <li><Link to='/about' className='blue-grey-text'>About</Link></li>
              <li><a href='/file/Sally Maki Resume - Junior Software Developer 2017.pdf' className='blue-grey-text' target='_blank'>Resume</a></li>
              <li><Link to='/contact' className='blue-grey-text'>Contact</Link></li>
            </ul>
            <a href="#" data-activates="nav-mobile" className=" button-collapse"><i className="material-icons grey-text">menu</i></a>
          </div>
        </nav>
        <RainbowHorizontal />
      </div>
    )
  }
}
