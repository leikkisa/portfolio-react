import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class SocialMedia extends Component {
  render() {
    return (
      <div className='social center'>
        <a href='https://github.com/leikkisa' target='_blank'>
          <i className='fa fa-github fa-lg'></i>
        </a>
        <a href='https://bitbucket.org/leikkisa/' target='_blank'>
          <i className='fa fa-bitbucket fa-lg'></i>
        </a>
        <a href='https://www.linkedin.com/in/sallymaki/' target='_blank'>
          <i className='fa fa-linkedin fa-lg'></i>
        </a>
        <a href='https://www.facebook.com/sallymaki' target='_blank'>
          <i className='fa fa-facebook fa-lg'></i>
        </a>
        <a href='https://twitter.com/leikkisa' target='_blank'>
          <i className='fa fa-twitter fa-lg'></i>
        </a>
        <a href='https://www.instagram.com/leikkissa/' target='_blank'>
          <i className='fa fa-instagram fa-lg'></i>
        </a>
        <Link to='/contact'>
          <i className='fa fa-envelope fa-lg'></i>
        </Link>
      </div>
    )
  }
}

export default SocialMedia
