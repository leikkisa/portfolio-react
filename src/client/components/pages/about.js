import React, { Component } from 'react'

export default class About extends Component {
  render () {
    return (
      <div className='container'>
        <h1>Who am I anyway?</h1>

        <p>This is me wondering how long I'll be able to feel my fingers (Michigan, Dec 2017).</p>
        <img src='/image/winter-sally.jpg' alt='Sally in Michigan for Christmas break' />
        <p>My dad was an environmentalist and a computer engineer, so I shouldn't be surprised that I turned out to be a tree-hugging geek.  My fifth-grade science project was evaluating different method for cleaning petroleum from seawater.  We had brine shrimp in each sample and kept track of how long they lived. Poor things.</p>

        <p>I became interested in clean energy, which eventually led me to the Energy and Resources Group at Berkeley.  My first job was on a renewables consulting team, where I became the go-to data person.  Next was a new analytics team at a PG&E, where I got to work with their rich internal data-ecosystem.  An emerging appetite for data-driven decision-making boosted our team's popularity, and much of our work developed into increasingly complex, ongoing processes.  My thinking shifted from "How do I answer this question?" to "How can I structure this workflow to be easier, better, and more resilient?".  I wanted to build systems where my time spent would be an investment in the future.
        </p>
        <p>Full of ideas I couldn't build with the resources I had, I started thinking about software development.  While perusing OkCupid (you never know when inspiration will strike), I found out about <a href="https://www.learnersguild.org/" target='_blank'>Learners Guild</a>, a new program for full-stack development.  The 3-month bootcamps always seemed too short, so I was excited.  I've been there since February, 2017 and am almost done!</p>

        <p>When I'm not getting cozy with the computer, I love books, philosophy, mac 'n cheese, and music festivals.  Oh, and my favorite color is rainbow <img src='/image/winky-face.svg' />.</p>

        <h2>About the site</h2>
        <p>
        This is a Node.js site with React, Redux, and Materialize on the front-end and Express and PostgreSQL on the back-end.  It has isomorphic rendering. Typefaces are <a href="https://fonts.google.com/specimen/Bellefair/" target="_blank">Bellefair</a>, <a href="https://fonts.google.com/specimen/Spectral/" target="_blank">Spectral</a>, and Optimus Princeps. Feel free to check out the code on <a href='https://bitbucket.org/leikkisa/portfolio-react' target='_blank'>
          Bitbucket&nbsp;
          <i className='fa fa-bitbucket'></i>
        </a>.
        </p>
        <h3>Photo Attributions</h3>
        <ul>
          <li>
            <a href="https://unsplash.com/photos/vdWewqfr_V0" target="_blank">Rainbow keyboard by Igor Ovsyannykov on Unsplash</a>
          </li>
        </ul>
      </div>
    )
  }
}
