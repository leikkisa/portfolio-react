import React, { Component } from 'react'
import Project from '../../projects/project'

export default class Projects extends Component {
  render () {
    return (
      <div className="row project">
        <div className="col s12 m12"><h1>Projects</h1></div>
        <div className="row">
        <Project image='/image/projects/roam.png'
        title='Roam'
        code='https://github.com/leikkisa/lg-roam'
        demo='https://lg-roam.herokuapp.com/'
        description='App where users can sign up, login, create a profile, add cities and reviews'
        reveal={<ul>
          <li>Node.js</li>
          <li>Express</li>
          <li>PostgreSQL</li>
          <li>Materialize</li>
          </ul>}
        />
        <Project image='/image/projects/sijo-min.png'
        title='Sijo'
        code='https://bitbucket.org/leikkisa/sijo'
        demo='https://sijo.surge.sh'
        description="Future website of a friend's clothing website, overall design provided by client"
        reveal={<ul>
          <li>HTML</li>
          <li>Sass</li>
          </ul>}
        />
        <Project image='/image/projects/weather-min.png'
        title='Weather'
        code='https://github.com/leikkisa/portfolio-react-redux-weather'
        demo='https://leikkisa.github.io/portfolio-react-redux-weather'
        description='React Redux app that charts OpenWeatherMap search results for multiple cities'
        reveal={<ul>
          <li>Node.js</li>
          <li>React</li>
          <li>Redux</li>
          <li>Bootstrap</li>
          <li>OpenWeatherMap API</li>
          <li>Google Maps API</li>
          </ul>}
        />
        <Project image='/image/projects/db-tweets-min.png'
        title='Retweet'
        code='https://github.com/leikkisa/lg-retweet'
        description='Bot that stores Twitter posts in a database and retweets them at a set interval'
        reveal={<ul>
          <li>Node.js</li>
          <li>PostgreSQL</li>
          <li>Twitter API</li>
          </ul>}
        />
        <Project image='/image/projects/inspiration-min.png'
        title='Inspiration'
        code='https://github.com/leikkisa/inspiration'
        demo='https://css-inspiration.surge.sh'
        description='Advanced CSS project with some fav (albeit cheesy) inspirational dummy content'
        reveal={<ul>
          <li>HTML</li>
          <li>Sass</li>
          </ul>}
        />
        <Project image='/image/projects/vinyl-home-min.png'
        title='Vinyl'
        code='https://github.com/leikkisa/lg-vinyl'
        demo='https://lg-vinyl.herokuapp.com/'
        description='CRUD app where users can sign up, login and submit reviews for available albums'
        reveal={<ul>
          <li>Node.js</li>
          <li>Express</li>
          <li>PostgreSQL</li>
          <li>Materialize</li>
          </ul>}
        />
        </div>
        <div className="row">
        <Project image='/image/projects/time-space-min.png'
        title='Space time'
        code='https://github.com/leikkisa/algonauts'
        description="Algorithms and a time and space calculator to compare solutions (I was curious)"
        reveal={<ul>
          <li>JavaScript</li>
          </ul>}
        />
        <Project image='/image/projects/movie-search-min.png'
        title='Movie Search'
        code='https://github.com/leikkisa/lg-movie-search-cli'
        description="Command line interface that searches IMDB and scrapes the results"
        reveal={<ul>
          <li>Node.js</li>
          <li>Command line</li>
          <li>Mocha/Chai</li>
          </ul>}
        />
        <Project image='/image/projects/trillo.png'
        title='Trillo'
        code='https://github.com/leikkisa/trillo'
        demo='https://leikkisa-trillo.surge.sh/'
        description="HTML and CSS Travel App Template built for practicing Flexbox"
        reveal={<ul>
          <li>Node.js</li>
          <li>Command line</li>
          <li>Mocha/Chai</li>
          </ul>}
        />
        </div>
      </div>
    )
  }
}
