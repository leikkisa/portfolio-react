import React, { Component } from 'react'

export default class Skills extends Component {
  render () {
    return (
      <div className='row'>
      <div className='col s12 m12'>
      <div className='card'>
      <div className="row flex skills-section">
        <div className="col s3 m2 skills-image">
          <h1>Skills</h1>
        </div>
        <div className="col s9 m10">
          <p><i className='fa fa-code fa-lg'></i> Node.js, JavaScript, SQL, HTML5, CSS3, Express, React, Git, Bash, EJS, Pug, Webpack</p>
          <p><i className='fa fa-book fa-lg'></i> Architecture, Restful routing, APIs, CRUD, Testing, HTTP, Authentication, Authorization, Data Structures, Algorithms, Database schemas and migrations, Deployment</p>
          <p><i className='fa fa-handshake-o fa-lg'></i> Stakeholder engagement, Scoping, Proposal development, Recruiting, Onboarding, Training, Process improvements, Documentation, Presenting to clients and leadership</p>
          <p><i className='fa fa-bar-chart fa-lg'></i> Advanced Excel modeling, VBA, SAS, ArcGIS, Tableau</p>
        </div>
        </div>
      </div>
      </div>
      </div>
    )
  }
}
