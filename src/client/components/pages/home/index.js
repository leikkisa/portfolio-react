import React, { Component } from 'react'
import Skills from './skills'
import PostsLatest from '../../posts/latest'
import Projects from './projects'

export default class Home extends Component {
  render () {
    return (
      <div className="container">
        <h1>Welcome!</h1>
        <p>I'm a data analyst turned software developer in Oakland, California, and am currently studying at <a href="http://www.learnersguild.org">Learners Guild</a>.  You've stumbled across my portfolio, which I plan on being a perpetual side-project. Hello! Say hi if you see me IRL :-).</p>
        <PostsLatest />
        <Projects />
        <Skills />
      </div>
    )
  }
}
