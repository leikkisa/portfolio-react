import React, { Component } from 'react'

export default class Contact extends Component {
  render() {
    return (
      <div className="container">
        <h1>Say hello!</h1>
        <p>Enough about me, I'd like to hear from you!  If you think we'd have something to talk about, feel free to reach out.</p>
        <h3>Email</h3>
        <p>
          <a href="mailto:sally.maki+web@gmail.com"><i className='fa fa-send'></i> sally.maki+web@gmail.com</a>
        </p>
        <h3>Phone</h3>
        <p>
          <a href="tel:415-448-6254"><i className='fa fa-mobile-phone fa-lg'></i> 415.448.6254</a>
        </p>
        <p>Note: Please leave a message, as I usually screen unexpected calls from unknown numbers.</p>
      </div>
    )
  }
}
