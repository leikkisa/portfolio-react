import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import Xkcd from './xkcd'

const randInt = Math.floor(Math.random() * 4 + 1).toString()

export default class NotFound extends Component {

  render () {

    return (
      <div className='container'>
        <h1>404: Not Found</h1>
        <Xkcd cKey={randInt} />
        <p>Go back <Link to='/'>home</Link> to find your way again.</p>
      </div>
    )
  }
}
