import React, { Component } from 'react'

const xkcdComics = {
  1: [170, "turn_back.png", "Just don't fall into the crack, and you'll find your way back to an existing page."],
  2: [1437, "higgs_boson.png", "That page might exist, but I'm having trouble finding it."],
  3: [71, "in_the_trees.jpg", "Not so fast. I'll find another real page!"],
  4: [246, "labyrinth_puzzle.png", "Do you like riddles? I'm sure you will find another existing page."]
}

export default class Xkcd extends Component {
  render () {
    // const cKey = this.props.cKey || 2
    const cKey = 2
    const randomComic = xkcdComics[cKey]
    const xkcdImgUrl = `https://imgs.xkcd.com/comics/${randomComic[1]}`
    const xkcdShareUrl = `https://xkcd.com/${randomComic[0]}`

    return (
      <div>
        <a href={xkcdShareUrl} target="_blank">
        <img src={xkcdImgUrl} className="image-block"/>
        {xkcdShareUrl}</a><br />
        <p>{randomComic[2]}</p>
      </div>
    )
  }
}
