import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class Thanks extends Component {
  render() {
    return (
      <div className="container">
        <h1>Thank you!</h1>
        <p>Your message has been sent. I might even be reading it right now, but give me a few moments to decipher.</p>
        <img src="/image/bunny-reading-email.jpg" className="image-block" />
        <p>Go back <Link to='/'>home</Link>.</p>
      </div>
    )
  }
}
