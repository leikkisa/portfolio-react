import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { contactMe } from '../../actions'

class Contact extends Component {
  renderField(field) {
    const { meta: { touched, error } } = field
    const className = `form-group ${touched && error ? 'has-danger' : ''}`
    return (
      <div className={className}>
        <label>{field.label}</label>
        <input
          className="form-control"
          type="text"
          {...field.input}
        />
        <div className="text-help">
          {touched ? error : ''}
        </div>
      </div>
    )
  }


    onSubmit(values) {
      this.props.contactMe(values, () => {
        this.props.history.push('/') // programatically navigate to a route
      })
    }

    render() {
      const { handleSubmit } = this.props

      return (
        <div>
          <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
            <Field
              label="Name"
              name="name"
              component={this.renderField}
            />
            <Field
              label="Email"
              name="email"
              component={this.renderField}
            />
            <Field
              label="Message"
              name="message"
              component={this.renderField}
            />
            <button type="submit" className="btn btn-primary">Submit</button>
            <Link to="/" className="btn btn-danger">Cancel</Link>
          </form>
        </div>
      )
    }
  }

  function ValidateEmail(inputText) {
    const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/

    if(inputText.value.match(mailformat)) {
      return true
    }
    else {
      return false
    }
  }

  function validate(values) {
    const errors = {}

    if (!values.name) {
      errors.name = "Don't forget your name!"
    }
    if (!values.email || !validateEmail(values.email)) {
      errors.email = 'I predict responding to that email might not work.'
    }
    if (!values.message || values.message.length < 10) {
      errors.message = "Don't forget to say something..."
    }

    return errors
  }

   export default reduxForm({
     validate,
     form: 'ContactNewForm'
   })(
     connect(null, { contactMe })(Contact)
   )
