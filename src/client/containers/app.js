import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'

import Header from '../components/shared/header'
import Navigation from '../components/shared/navigation'
import Footer from '../components/shared/footer'
import SocialMedia from '../components/shared/social-media'
import Home from '../components/pages/home'
import About from '../components/pages/about'
import Contact from '../components/pages/contact'
import Thanks from '../components/pages/thanks'
import NotFound from '../components/pages/not-found'

import PostsIndex from '../components/posts/index'
import PostsNew from '../components/posts/new'
import PostsShow from '../components/posts/show'
import PostsLatest from '../components/posts/latest'

export default class App extends Component {
  render() {
    return (
      <div className='app'>
        <Navigation />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/about" component={About} />
          <Route exact path="/contact" component={Contact} />
          <Route exact path="/thanks" component={Thanks} />
          <Route path="/posts/new" component={PostsNew} />
          <Route path="/posts/latest" component={PostsLatest} />
          <Route path="/posts/:id" component={PostsShow} />
          <Route path="/posts" component={PostsIndex} />
          <Route component={NotFound} />
        </Switch>
        <Footer />
      </div>
    )
  }
}
