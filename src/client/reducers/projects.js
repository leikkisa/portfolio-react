import _ from 'lodash'
import { FETCH_PROJECTS, FETCH_PROJECT, DELETE_PROJECT } from '../actions'

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_PROJECT:
      return { ...state, [action.payload.data.id]: action.payload.data }
    case FETCH_PROJECT:
      return _.mapKeys(action.payload.data, 'id')
    case DELETE_PROJECT:
      return _.omit(state, action.payload)
    default:
      return state
  }
}
