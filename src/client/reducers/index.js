import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import projectsReducer from './projects'
import postsReducer from './posts'

const rootReducer = combineReducers({
  projects: projectsReducer,
  posts: postsReducer,
  form: formReducer
})

export default rootReducer
