CREATE TABLE post (
  id serial PRIMARY KEY,
  title varchar(255) NOT NULL,
  author varchar(255) default NULL,
  categories json default NULL,
  created_date timestamp default current_timestamp,
  modified_date timestamp default current_timestamp,
  content text NOT NULL,
  photo_url varchar(255) default NULL,
  thumbnail_url varchar(255) default NULL,
  published boolean default NULL,
  sort_order integer default 1
);

CREATE TABLE project (
  id serial PRIMARY KEY,
  title varchar(255) NOT NULL,
  categories json default NULL,
  stack json default NULL,
  skills json default NULL,
  created_date timestamp default current_timestamp,
  description text NOT NULL,
  photo_url varchar(255) default NULL,
  thumbnail_url varchar(255) default NULL,
  repo_url varchar(255) default NULL,
  live_url varchar(255) default NULL,
  published boolean default FALSE,
  sort_order integer default 1
);

CREATE TABLE quotation (
  id serial PRIMARY KEY,
  author varchar(255) default NULL,
  categories json default NULL,
  quotation text NOT NULL,
  submitted_by varchar(255) default NULL,
  published boolean default NULL,
  created_date timestamp default current_timestamp
);
